package com.example.service.repository

import com.example.service.model.TestEntity
import org.springframework.data.jpa.repository.JpaRepository
import java.util.UUID

interface TestEntityRepository : JpaRepository<TestEntity, UUID>
