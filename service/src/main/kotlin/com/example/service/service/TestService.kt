package com.example.service.service

import com.example.service.model.TestEntity
import com.example.service.repository.TestEntityRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class TestService(private val testEntityRepository: TestEntityRepository) {


  fun getById(id: UUID) = testEntityRepository.getReferenceById(id)

  fun deleteById(id: UUID) = testEntityRepository.deleteById(id)

  fun upsert(testEntity: TestEntity): TestEntity {
    return testEntityRepository.save(testEntity)
  }


}
