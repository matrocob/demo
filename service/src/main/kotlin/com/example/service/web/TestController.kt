package com.example.service.web

import com.example.service.model.TestEntity
import com.example.service.service.TestService
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping("/test")
class TestController(private val testService: TestService) {


  @GetMapping("/{id}")
  fun getById(@PathVariable id: UUID) = testService.getById(id)

  @DeleteMapping("/{id}")
  fun deleteById(@PathVariable id: UUID) = testService.deleteById(id)

  @PostMapping
  fun upsert(@RequestBody testEntity: TestEntity) = testService.upsert(testEntity)

}
